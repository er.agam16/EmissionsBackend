"""Save the trends file into json"""

from db_utilities import *
import json

#get trending data
trend_data = get_trend_emissions_yearly()


# open the file named students_all
file = open('trends.json', "w+")

print('File being created for all students in json format')

# loads the json output to file
json.dump(trend_data, file)

# Close the file
file.close()

print('File creation successful for all students in json format')
