#   Import the required modules
import pyodbc
import sys
from Connection.credentials import *

# create the database connection class
class DatabaseConnection(object):
    """A database object model:"""

    def __init__(self):
        self.db_connection = None

    # Connect DB method which returns the db connection object 
    def connect_db(self):
        connection_string = ("Driver={};Server={};Database={};Uid={};Pwd={};{}").format(DRIVER_NAME, SERVER_NAME, DATABASE_NAME, UID, PWD,ADD_COMMANDS)
        try:
            self.db_connection = pyodbc.connect(connection_string)
            return self.db_connection
        except Exception as exp:
            print(exp)
            sys.exit('An Exception occurred during the connection')

    # Retrieves the DB instance which returns the db connection object 
    def get_db(self):
        if self.db_connection and self.db_connection.open:
            return self.db_connection
        else:
            return self.connect_db()

    # Close DB method which returns the true if successfully closed
    def close_db(self):
        try:
            self.db_connection.close()
            return True
        except Exception as exp:
            print(exp)
            sys.exit('An Exception occurred during the closing')
