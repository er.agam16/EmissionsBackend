"""Import all the required modules"""
from flask import Flask, request
from flask_restful import Resource, Api
import json
from flask_cors import CORS
from db_utilities import *
from datetime import date, datetime

#create the flask app
app = Flask(__name__)
api = Api(app)
CORS(app)

#get trends by year
@app.route("/api/getTrendsByYear")
def getTrendsByYear():
    with open('trends.json') as json_data:
        d = json.load(json_data)
    return json.dumps(d)

# get projected data based on year 
@app.route("/api/getProjectedByYear")
def getProjectedByYear():
    return json.dumps(get_projected_trend())

#get list of top emittors
@app.route("/api/top_10_emitters")
def top_ten_emittors():
    return json.dumps(top_ten_emittors_from_database())

#get list of top non emittors
@app.route("/api/top_10_non_emitters")
def top_ten_non_emittors():
    return json.dumps(top_ten_non_emittors_from_database())

#get list of change in population
@app.route("/api/change_in_population")
def change_in_population():
    return json.dumps(get_change_in_population())

#get traits of top emittors
@app.route("/api/sectors_top_ten_emittors")
def sectors_top_ten_emittors():
    return json.dumps(get_sectors_top_ten_emittors())

#get traits of top non emittors
@app.route("/api/sectors_top_ten_non_emittors")
def sectors_top_ten_non_emittors():
    return json.dumps(get_sectors_top_ten_non_emittors())

#get list of latest news
@app.route("/api/latest_news")
def latest_news():
    return json.dumps(get_latest_news(), default=json_serial)


#get list of latest keywords
@app.route("/api/latest_keywords")
def latest_keywords():
    return json.dumps(get_latest_keywords())


#get list of yearwise gas emissions for all countries
@app.route("/api/create_json_topo")
def create_json_topo():
    json_top_data = json_topo()
    return json.dumps(json_top_data)


#serialize into json
def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))

#run the server
if __name__ == '__main__':
     app.run(host= '0.0.0.0', port=5000, threaded=True)
     