"""Scrap the website"""

#import the required modules for the scraping parts
from bs4 import BeautifulSoup
from custom_req import *
from datetime import datetime
import newspaper
from newspaper import Article
from db_utilities import *

# retrives the HTML of the website passed in the simple_get method
get_html = simple_get('https://www.theguardian.com/environment/climate-change')

#   pass the recieved html to Beautiful Soup so we can traverse the html classess
html = BeautifulSoup(get_html, 'html.parser')

# select the article class
selected_class = html.find("div", {"class": "u-cf index-page"})
#find the news section in the selected class
news_section = selected_class.find_all("section", {"class" : "fc-container fc-container--tag "})

# initialize the last_date to None
# it will be used to save the news only after the last saved news in the database
# to prevent duplicate records
last_date = None
last_record = get_last_news()
if last_record:
    last_date = last_record[3]

counter = 0
#iterating on the news section to save the database
for date_div in news_section:
    date_published = date_div['id']
    date_published = datetime.strptime(date_published, '%d-%B-%Y')
    date_published = date_published.date()
    #if we have the last date as latest then stop the scraping
    if last_date and date_published <= last_date:
        print("Everything is already upto date")
        break
    all_news = date_div.find_all("li", {"class" : "fc-slice__item"})
    #iterate the news & save it into database
    for news in all_news:
        news_link = news.find("a", {"class": "u-faux-block-link__overlay js-headline-text"})
        if news_link:
            news_link_text = news_link.get('href', None)
            if news_link_text:
                art = Article(news_link_text)
                art.download()
                art.parse()
                art.nlp()
                save_to_news(art.title, art.summary, date_published, ','.join(art.keywords))
                counter = counter + 1
                print("Saved News Counter :- " + str(counter))
                #sleep the code for 4 seconds
                import time
                time.sleep(4)




